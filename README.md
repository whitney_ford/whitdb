# WhitDB README
## Author
Whitney Ford 
## What is this repository for?
This is a redis inspired (key, value) database, utilizing the memento and command design patterns. 
## Software 
Java 8
## How do I get set up?
Clone or download zip for repository.  From the command line cd into your local repository's directory and run **mvn exec:java**.  Start up the whitdb-client to execute commands.