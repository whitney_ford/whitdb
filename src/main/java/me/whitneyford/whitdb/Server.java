package me.whitneyford.whitdb;

//IO
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.IOException;

//NETWORK
import java.net.ServerSocket;
import java.net.Socket;

//CONCURRENT
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import me.whitneyford.whitdb.WhitDB.*;


/* The Server in the Client-Server Architecture.  Server connects to the client
 * and executes commands. */

public class Server extends Thread{
	
	
	private static  ServerSocket sSocket;
	private static Socket sharedSocket;
	private static BufferedReader inputStream;
	private static PrintWriter outputStream;
	private static WhitDB whitDB;
	private static ThreadPoolExecutor executor;
	
	//Default values
	private static int port = 6700;
	private static boolean isTransaction = false;
	private static int transDepth = 0;
	

	public Server(int port){		
		 
		try {
			//create server socket
			sSocket = new ServerSocket(port);
			
			//create thread pool executor
			executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static String processCommand (String inputLine) throws InterruptedException, ExecutionException{
		String [] commandLine;
		Callable<String> inputCommand;

		commandLine = inputLine.split(" ");	
		
		if(commandLine.length == 3){					
			if(isTransaction)
				inputCommand = new TransactionCommand(commandLine[0], commandLine[1], commandLine[2], isTransaction, transDepth);	

			else inputCommand =  new DataCommand(commandLine[0], commandLine[1], commandLine[2]);				

		}else if(commandLine.length == 2){

			if(commandLine[0].toUpperCase().equals("COMMAND"))
				commandLine[0] = commandLine[0] + " " + commandLine[1];

			if(isTransaction)						
				inputCommand = new TransactionCommand(commandLine[0],commandLine[1], isTransaction, transDepth);

			else inputCommand = new DataCommand(commandLine[0], commandLine[1]);

		}else if(commandLine.length == 1){
			if(isTransaction){
				inputCommand = new TransactionCommand(commandLine[0], isTransaction, transDepth);

			}else {
				if(commandLine[0].equals("BEGIN")){
					inputCommand = new TransactionCommand(commandLine[0], isTransaction, transDepth);
					isTransaction = true;

				}else inputCommand = new DataCommand(commandLine[0]);
			}

		}else{
			return "";
		}
		return executor.submit(inputCommand).get();		
	}
	
	public void run (){
		
		String inputLine, outputLine;
		
		try {
			
			System.out.println("Server started.  Waiting for clients to connect.");
			
			//accept client socket 
			sharedSocket = sSocket.accept();
			
			//initiate input and output streams 
			inputStream = new BufferedReader(new InputStreamReader(sharedSocket.getInputStream()));
			outputStream = new PrintWriter(new OutputStreamWriter (sharedSocket.getOutputStream()), true);
			
			//initiate  whitdb
			whitDB = new WhitDB();		
			
			outputStream.println("Server now connected to client on port " + port);			
						
			while((inputLine = inputStream.readLine()) != null){			
				
				System.out.println("inbound message: " + inputLine);
				outputLine = processCommand(inputLine);
				
				System.out.println("outbound message: " + outputLine);
				outputStream.println(outputLine);			
			}
						
		} catch (IOException e) {			
			System.err.println("Having trouble connecting to port " + port + ".");
			System.exit(-1);
			
		} catch( InterruptedException e) {
			System.err.println("Could not execute command.");
			
		} catch (ExecutionException e){
			System.err.println("Could not execute command.");
		}		
	}
	
	public static void main(String [] args){
		if(args.length > 0)
			port = Integer.parseInt(args[0]);
		
		Server server = new Server(port);
		server.start();
		System.out.println("Server Thread starting...");
	}	
}
