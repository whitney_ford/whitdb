package me.whitneyford.whitdb;

import java.util.ArrayList;

public class CareTaker{
	
	private ArrayList<Memento> states = new ArrayList<Memento>();
	
	public void addMemento(Memento m){
		states.add(m);
	}
	
	public Memento getMemento(int index){
		return states.get(index);
	}
	
	public int size(){
		return states.size();
	}
	public void printCareTaker(){
		for(Memento m : states){
			System.out.println("dataMap: " + m.getState().getDataMap());
			System.out.println("valueMap " + m.getState().getValueMap());
			System.out.println("=====");
		}
	}
}
