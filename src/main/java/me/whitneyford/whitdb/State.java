package me.whitneyford.whitdb;

import java.util.Hashtable;

public class State implements Cloneable {
	private Hashtable<String, String> dataMap;
	private Hashtable<String, Integer> valueMap;
	
	public State(){
		dataMap = new Hashtable<String, String>();
		valueMap = new Hashtable<String, Integer>();
	}
	
	public Hashtable<String, String> getDataMap(){
		return dataMap;	
	}
	
	public Hashtable<String, Integer> getValueMap(){
		return valueMap;
	}
	
	public void increaseOccurrence(String key){
		Integer value = valueMap.get(key);
		if(value == null)
			valueMap.put(key, 1);
		else valueMap.put(key, value + 1);
	}
	
	public void decreaseOccurrence(String key){
		Integer value = valueMap.get(key);
		if(value == null)
			return;
		else if(value == 1)
			valueMap.remove(key);
		else
			valueMap.put(key, value - 1);	
	}
	
	protected Object clone() throws CloneNotSupportedException{
		State copy = (State) super.clone();
		copy.dataMap = (Hashtable<String, String>) dataMap.clone();
		copy.valueMap = (Hashtable<String, Integer>) valueMap.clone();
		return copy;
	}
}
