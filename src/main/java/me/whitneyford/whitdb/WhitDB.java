package me.whitneyford.whitdb;

//CONCURRENT
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/** 
 * 
 * @author Whitney Ford (whitneyforddevATgmail.com)
 * Redis inspired key-value stripped-down database.  Utilizes the Memento  and Command 
 * via Callable interface) design patterns. 
 *
 */

public class WhitDB {	

	private static int commandCount;
	private static Originator db;
	private static CareTaker caretaker; 

	public WhitDB(){	
		commandCount = 0;
		db = new Originator();
		caretaker = new CareTaker();	
	}
	
	public static class DataCommand implements Callable<String>  {

		private State instance;
		private String command;
		private String key;
		private String value;

		public DataCommand(String command, String key, String value){
			this.command = command.toUpperCase();
			this.key = key;
			this.value = value;
		}

		public DataCommand(String command, String keyOrValue){
			this.command = command.toUpperCase();

			if(command.equals("NUMEQUALTO"))
				this.value = keyOrValue;
			else
				this.key = keyOrValue;
		}

		public DataCommand(String command){
			this.command = command.toUpperCase();
		}

		@Override
		public String call() throws InterruptedException {
			String result = "";
			
			try{
				switch(command){
				case "SET":
					instance = (State) db.getState().clone();	
					String prevValue = instance.getDataMap().get(key);
					if(prevValue == null){
						instance.increaseOccurrence(value);
						instance.getDataMap().put(key, value);					
					}else if( ! (prevValue.equals(value))){
						instance.decreaseOccurrence(prevValue);
						instance.increaseOccurrence(value);
						instance.getDataMap().put(key, value);
					}else{
						instance.increaseOccurrence(value);
						instance.getDataMap().put(key, value);
					}
					commandCount ++;
					db.setMemento(instance);
					caretaker.addMemento(db.createMemento());
					break;
				case "UNSET":
					instance = (State) db.getState().clone();
					String occurKey = instance.getDataMap().get(key);
					if(occurKey != null){
						instance.getDataMap().remove(key);
						instance.decreaseOccurrence(occurKey);
					}
					commandCount++;
					db.setMemento(instance);
					caretaker.addMemento(db.createMemento());
					break;
				case "GET":
					commandCount++;
					String val = db.getState().getDataMap().get(key);
					result = (val == null ? "NULL" : val);
					break;
				case "NUMEQUALTO":
					commandCount++;
					Integer occurrence = db.getState().getValueMap().get(value);
					result = (occurrence == null ? "0" : occurrence.toString());
					break;
				case "END":
					commandCount++;
					break;
				case "COMMAND COUNT":
					result = "(integer)" + " " + commandCount;
					break;
				}

			}catch(CloneNotSupportedException e){
				e.printStackTrace();
			}
			TimeUnit.SECONDS.sleep(2);
			return result;
		}
		
	}

	public static class TransactionCommand implements Callable<String> {
		private State instance;
		private String command;
		private String key;
		private String value;
		private boolean isTransaction;
		private int transDepth;

		public TransactionCommand(String command, String key, String value, boolean isTransaction, int transDepth){
			this.command = command.toUpperCase();
			this.key = key;
			this.value = value;
			this.isTransaction = isTransaction;
			this.transDepth = transDepth;
		}

		public TransactionCommand(String command, String keyOrValue, boolean isTransaction, int transDepth){
			this.command = command.toUpperCase();
			this.isTransaction = isTransaction;
			this.transDepth = transDepth;

			if(command.equals("NUMEQUALTO"))
				this.value = keyOrValue;
			else
				this.key = keyOrValue;
		}

		public TransactionCommand(String command, boolean isTransaction, int transDepth){
			this.command = command.toUpperCase();
			this.isTransaction = isTransaction;
			this.transDepth = transDepth;
		}

		@Override
		public String call() throws InterruptedException {
			
			String result = "";
			try{
				switch(command){
				case "BEGIN":
					if(transDepth == 0){
						transDepth = 1;
						instance = (State) db.getState().clone();
						db.setMemento(instance);										
					}else{
						transDepth++;				
						caretaker.addMemento(db.createMemento()); //save the current state
						instance = (State) db.getState().clone(); //create new instance & set memento
						db.setMemento(instance);
					}
					commandCount++;
					break;

				case "SET":	
					instance = (State) db.getState();
					String prevValue = instance.getDataMap().get(key);
					if(prevValue == null){
						instance.increaseOccurrence(value);
						instance.getDataMap().put(key, value);
						db.setMemento(instance);
					}else if( ! (prevValue.equals(value))){
						instance.decreaseOccurrence(prevValue);
						instance.increaseOccurrence(value);
						instance.getDataMap().put(key, value);
						db.setMemento(instance);
					}else{
						instance.increaseOccurrence(value);
						instance.getDataMap().put(key, value);
						db.setMemento(instance);
					}	
					commandCount++;
					break;
				case "UNSET":
					instance = (State) db.getState();
					String occurKey = instance.getDataMap().get(key);
					if(occurKey != null){
						instance.getDataMap().remove(key);
						instance.decreaseOccurrence(occurKey);
					}
					commandCount++;
					db.setMemento(instance);
					break;
				case "GET":
					String val = db.getState().getDataMap().get(key);
					result = (val == null ? "NULL" : val);
					commandCount++;
					break;
				case "NUMEQUALTO":
					Integer occurrence = db.getState().getValueMap().get(value);
					result = (occurrence == null ? "0" : occurrence.toString());
					commandCount++;
					break;
				case "END":
					//some halt - thinking system.exit(0)
					commandCount++;
					break;
				case "ROLLBACK":
					if(isTransaction){
						if(caretaker.size() < 1)
							result = "NO TRANSACTION";
						else db.setMemento(db.restore(caretaker.getMemento(caretaker.size() - 1)));
						transDepth--;
					} else result = "NO TRANSACTION";	
					commandCount++;
					break;				
				case "COMMIT":					
					if(isTransaction){
						isTransaction = false;
						caretaker.addMemento(db.createMemento());//create memento & save state
					}
					else result = "NO TRANSACTION";		
					transDepth = 0;
					commandCount++;
					break;
				case "COMMAND COUNT":
					result = "(integer)" + " " + commandCount;
					break;
				}

			}catch(Exception e){
				e.printStackTrace();
			}
			TimeUnit.SECONDS.sleep(2);
			return result;
		}

	}
}
