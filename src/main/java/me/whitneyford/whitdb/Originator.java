package me.whitneyford.whitdb;

public class Originator {
	
	private State db;
	
	public Originator(){
		db = new State();
	}
	public State getState(){
		return db;
	}
	
	public void setMemento(State db){
		this.db = db;
	}
	
	public Memento createMemento(){
		return new Memento(db);
	}
	
	public State restore(Memento m){
		return m.getState();
	}
}
